using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System;



namespace EFAccount
{


    /* 
        1) What will be the structure of your code? (code file, class structures, etc)
            IN ORDER TO DEVELOP THE BANKING APPLICATION, CLASSES OF DbContext SHOULD BE 
            INHERITED WITHIN THE Model.cs FILE. THE Model.cs FILE WILL BE USED TO CREATE
            THE DIFFERENT CLASS STRUCTURES NECCESSARY FOR THE APPLICATION TO BE LINKED TO 
            THE DATABASE. THE Model.cs FILE IS THE LINK BETWEEN CLASS STRUCTURES AND THE 
            DATABASE. THE CLASS STRUCTURES SHOULD BE THE MAIN CONSTRUCTS IN THE Model.cs
            FILE WHICH SHOULD BE CREATED BASED ON THE FUNCTION OF THE APPLICATION.

        2) What are the C# constructs/concepts that you think you might use and why?
            THE MAIN C# CONSTRUCTS INCLUDES: DOTNET ENTITY FRAMEWORK CORE TO CREATE 
            CLASSES LINKED TO A DATABASE, INHERITANCE CONCEPT: IN ORDER TO CREATE A DATABASE 
            THE DbContext CLASS SHOULD BE INHERITED TO MAKE THE CONNECTION TO THE 
            DATABASE, PROPERTIES: WILL BE A CONCEPT TO USED WITHIN THE CLASSES, 
            LISTS: THE CREATION OF LISTS MAY ALSO BE USED AND THE CREATION OF 
            DIFFERENT ENTITIES WILL ALSO BE USED.

    */

    public class AccountContext : DbContext
    {
        public DbSet<Person> Person { get; set; }

        public DbSet<Account> Account { get; set; }
        public DbSet<Transaction> Transaction { get; set; }



        //Constructor
        public AccountContext(DbContextOptions<AccountContext> options) : base(options) { }
        public AccountContext() : base() { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source= bankingApplicationFour.db");
        }

    }


    public class Person
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public char Gender { get; set; }


        public int PersonID { get; set; }  // [PrimaryKey]

    }

    public class Account
    {
        private string _accountStatus;
        public string AccountType { get; set; }
        public int AccountNumber { get; set; }
        public decimal AccountBalance { get; set; }
        public string AccountStatus
        {
            get
            {
                return _accountStatus;
            }

            set
            {
                _accountStatus = value;

            }
        }
        //status(Active, Closed)
        public int AccountID { get; set; }// [PrimaryKey]
        public int PersonID { get; set; }// [ForeignKey]

        public Person Person { get; set; }

    }



    public class Transaction
    {
        public string AccountType { get; set; }
        public decimal Amount { get; set; }
        public int TransactionID { get; set; }// [PrimaryKey]
        public string TransactionType { get; set; }//(Withdrawal, Deposit)

        public DateTime TransactionDate { get; set; }
        public int AccountID { get; set; }
    }


}
