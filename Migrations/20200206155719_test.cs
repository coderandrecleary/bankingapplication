﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace bankingapplication.Migrations
{
    public partial class test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Account_PersonID",
                table: "Account",
                column: "PersonID");

            //     migrationBuilder.AddForeignKey(
            //         name: "FK_Account_Person_PersonID",
            //         table: "Account",
            //         column: "PersonID",
            //         principalTable: "Person",
            //         principalColumn: "PersonID",
            //         onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Account_Person_PersonID",
                table: "Account");

            migrationBuilder.DropIndex(
                name: "IX_Account_PersonID",
                table: "Account");
        }
    }
}
