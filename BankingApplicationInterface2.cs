using System;
using EFAccount;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using System.Text;
using System.Linq.Expressions;



namespace BankApplicationInterface
{

    public class BankApplicationInterface
    {


        public static void Main(string[] args)
        {

            BankApplicationInterface app = new BankApplicationInterface();
            app.startBankingApplication();

        }



        public void showMainMenu()
        {


            Console.WriteLine("---------------------DISPLAY MAIN MENU------------------------");
            Console.WriteLine("---------------------SELECT MENU OPTION-----------------------");
            Console.WriteLine("*****************1) OPEN ACCOUNT******************************");
            Console.WriteLine("*****************2) NEW TRANSACTION***************************");
            Console.WriteLine("*****************3) VIEW ACCOUNT******************************");
            Console.WriteLine("*****************4) CLOSE AN ACCOUNT**************************");
            Console.WriteLine("**************quit) EXIT APPLICATION**************************");

        }

        public void showMainMenuTwo()
        {

            Console.WriteLine("Please specify the type of transaction - Options are: Withdrawal/Deposit.");
            Console.WriteLine("2.1 Withdrawal or 2.2 Deposit.");

        }



        public void openAccount(AccountContext db)
        {

            try
            {

                Console.WriteLine("PLEASE ENTER PERSONAL INFORMATION");
                Console.WriteLine("Enter First Name:");
                string firstName = Console.ReadLine();
                Console.WriteLine("Enter Last Name:");
                string lastName = Console.ReadLine();

                Console.WriteLine("Please Enter Date Of Birth:");
                Console.WriteLine("Enter the Year:");
                string yearEntered = Console.ReadLine();

                Console.WriteLine("Enter the Month:");
                string monthEntered = Console.ReadLine();

                Console.WriteLine("Enter the Date:");
                string dateEntered = Console.ReadLine();

                string dateOfBirth = yearEntered + "/" + monthEntered + "/" + dateEntered;
                CultureInfo en = new CultureInfo("en-US");
                DateTime dob = Convert.ToDateTime(dateOfBirth, en);


                Console.WriteLine("Please Enter Gender: M/F");
                char gender = Console.ReadLine()[0];

                if (gender == 'M' || gender == 'F') { }
                else
                {
                    Console.WriteLine("The character you entered was not recognized as a valid gender.");
                    return;

                }


                Console.WriteLine("ENTER ACCOUNT INFORMATION:");
                Console.WriteLine("Please enter Account Type (Savings/Checking):");
                string accountType = Console.ReadLine();
                if (accountType == "Savings" || accountType == "Checking" || accountType == "SAVINGS" || accountType == "CHECKING" || accountType == "checking" || accountType == "savings")
                {

                }
                else if (accountType == "S" || accountType == "s" || accountType == "C" || accountType == "c")
                {
                    Console.WriteLine("The account type that you entered is not valid.");
                    Console.WriteLine("Only Savings/Checking Accounts are accepted.");
                    return;
                }

                Console.WriteLine("Enter Opening Account Balance: ");
                Console.Write("$");
                decimal openingAccountBalance = Convert.ToDecimal(Console.ReadLine());
                openingAccountBalance.ToString("C");

                var currentPerson = new Person
                {
                    FirstName = firstName,
                    LastName = lastName,
                    DOB = dob,
                    Gender = gender,

                };

                db.Person.Add(currentPerson);

                try
                {

                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error while adding personal information:{0}", e.GetInnerMessages());
                }

                //Update
                Random random = new Random();
                int account_Number = random.Next(10000, 99999);
                string active = "Active";

                var accountInfo = new Account
                {
                    AccountType = accountType,
                    AccountNumber = account_Number,
                    AccountBalance = openingAccountBalance,
                    AccountStatus = active,
                    PersonID = currentPerson.PersonID

                };

                db.Account.Add(accountInfo);

                try
                {
                    db.SaveChanges();

                    string AccountStatus = active;
                    Console.WriteLine("Account record was successfully saved.");
                    Console.WriteLine("Here is your Account Number: " + account_Number);


                }
                catch (Exception e)
                {
                    Console.WriteLine(e.GetInnerMessages());
                    Console.WriteLine("Account record was not successfully saved.\n" + e.GetInnerMessages());
                    Console.WriteLine("Please try again...");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The month or the date you entered was not recognized as a valid DateTime.");
                return;
            }
        }


        public void withdrawFromAccount(AccountContext db)
        {

            try
            {
                Console.WriteLine("Please enter your Account Number:");
                string enteredAccountNumber = Console.ReadLine();
                int Account_Number = Convert.ToInt32(enteredAccountNumber);

                Console.WriteLine("Enter Account Type. Options are: Savings/Checking");
                string acc_Type = Console.ReadLine();


                if (acc_Type == "Savings" || acc_Type == "Checking" || acc_Type == "SAVINGS" || acc_Type == "CHECKING" || acc_Type == "checking" || acc_Type == "savings")
                {

                }
                else if (acc_Type == "S" || acc_Type == "s" || acc_Type == "C" || acc_Type == "c")
                {
                    Console.WriteLine("The account type that you entered is not valid.");
                    Console.WriteLine("Only Savings/Checking Accounts are accepted.");
                    return;
                }

                string closedStatus = "Closed";

                var accountQuery = db.Account.Where(Account => Account.AccountNumber == Account_Number);
                var account = accountQuery.FirstOrDefault();

                if (account == null)
                {
                    Console.WriteLine($"Account Number you entered does not exist.");
                    return;
                }


                if (accountQuery.Count() > 1)
                {
                    Console.WriteLine("End Process.");
                    return;
                }

                if (account.AccountStatus == closedStatus)
                {
                    Console.WriteLine("Account Status is Closed. Cannot do a withdrawal.");
                    Console.WriteLine("End Process.");
                    return;
                }

                Console.WriteLine("Please enter withdrawal amount:");
                Console.Write("$");
                string amount = Console.ReadLine();
                decimal withdrawalAmount = Convert.ToDecimal(amount);

                if (account.AccountBalance < withdrawalAmount)
                {
                    Console.WriteLine("Account Balance is less than withdrawal amount.");
                    Console.WriteLine("End Process");
                    return;
                }

                // reduce account balance
                account.AccountBalance -= withdrawalAmount;

                // Create and store transaction
                var withdrawalTranaction = new Transaction
                {
                    AccountID = account.AccountID,
                    TransactionType = "Withdrawal",
                    Amount = withdrawalAmount,
                    TransactionDate = DateTime.Now,
                    AccountType = acc_Type
                };

                db.Transaction.Add(withdrawalTranaction);

                try
                {
                    db.SaveChanges();
                    Console.WriteLine("Inserting Transaction");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"The Account Number you entered does not exist.");


                }

                Console.WriteLine($" Current Balance: {account.AccountBalance.ToString("C")} ");

            }
            catch (Exception e)
            {
                Console.WriteLine($"The Account Number you entered does not exist.");
            }
        }



        public void depositToAccount(AccountContext db)
        {
            try
            {
                Console.WriteLine("Enter Account Number:");
                string account_Number = Console.ReadLine();
                int accountNumber = Convert.ToInt32(account_Number);

                Console.WriteLine("Enter Account Type. Options are: Savings/Checking");
                string accountType = Console.ReadLine();

                if (accountType == "Savings" || accountType == "Checking" || accountType == "SAVINGS" || accountType == "CHECKING" || accountType == "checking" || accountType == "savings")
                {

                }
                else if (accountType == "S" || accountType == "s" || accountType == "C" || accountType == "c")
                {
                    Console.WriteLine("The account type that you entered is not valid.");
                    Console.WriteLine("Only Savings/Checking Accounts are accepted.");
                    return;
                }

                var account = db.Account.FirstOrDefault(account => account.AccountNumber == accountNumber);



                string activeStatus = "Active";
                string closedStatus = "Close";

                if (account.AccountNumber != accountNumber)
                {
                    Console.WriteLine("Account Number does not exist.");
                    return;
                }

                if (account.AccountStatus == closedStatus)
                {
                    Console.WriteLine("Account Status is Closed. Cannot do a Deposit.");
                    Console.WriteLine("End Process.");
                    return;
                }

                DateTime DateTime = new DateTime();
                string transactionType = "deposit";

                Console.WriteLine("Enter the amount you would like to deposit for the Transaction: ");
                Console.Write("$");
                string amountString = Console.ReadLine();
                decimal amount = Convert.ToDecimal(amountString);

                var currentTransaction = new Transaction
                {
                    Amount = amount,
                    TransactionDate = DateTime.Now,
                    AccountID = account.AccountID,
                    TransactionType = transactionType,
                    AccountType = accountType
                };

                account.AccountBalance += amount;

                db.Transaction.Add(currentTransaction);
                db.SaveChanges();
                Console.WriteLine("Transaction Information Was Inserted.");
                Console.WriteLine($" Current Balance: {account.AccountBalance.ToString("C")} ");
            }
            catch (Exception e)
            {
                Console.WriteLine($"The Account Number you entered does not exist.");

            }
        }




        public void viewAccount(AccountContext db)
        {
            try
            {

                Console.WriteLine("Enter Account Number:");
                string accounNumberString = Console.ReadLine();
                int accountNumber = Convert.ToInt32(accounNumberString);

                var account = db.Account.Include(x => x.Person).SingleOrDefault(x => x.AccountNumber == accountNumber);

                if (account == null)
                {
                    Console.WriteLine($"Account Number you entered does not exist.");
                    return;
                }

                Console.WriteLine("\nAccount Number exists.");
                Console.WriteLine("Displaying Account Information:\n");

                string s = "AccountNumber: {0}\n AccountType: {1}\nAccountBalance: {2}\nFirstName: {3}\nLastName: {4}\nPersonID: {5}\nAccountID:{6}";
                string info = String.Format(s, account.AccountNumber, account.AccountType, account.AccountBalance.ToString("C"), account.Person.FirstName, account.Person.LastName, account.Person.PersonID, account.AccountID);
                Console.WriteLine(info);
            }
            catch (Exception e)
            {
                Console.WriteLine($"The Account Number you entered does not exist.");

            }
        }


        public void closeAccount(AccountContext db)
        {
            try
            {
                string statusActive = "Active";
                string statusClose = "Close";

                Console.WriteLine("Enter Account Number: ");
                string accountNum = Console.ReadLine();
                int accountNumber = Convert.ToInt32(accountNum);

                Console.WriteLine("Enter Reason For Closure");
                string closure = Console.ReadLine();

                var account = db.Account.FirstOrDefault(account => account.AccountNumber == accountNumber);

                if (account == null)
                {
                    Console.WriteLine($"Account Number you entered does not exist.");
                    return;
                }


                if (account.AccountStatus != statusActive)
                {
                    Console.WriteLine("Account is already Closed.");
                    Console.WriteLine("End Process.");
                    return;
                }

                if (account.AccountNumber == accountNumber && account.AccountStatus == statusActive)
                {
                    account.AccountStatus = statusClose;
                    db.SaveChanges();


                    Console.WriteLine($"Account {accountNumber} is now Closed.");

                }
            }
            catch (Exception e)
            {

                Console.WriteLine($"The Account Number you entered does not exist.");
            }

        }



        public void startBankingApplication()
        {

            using (var db = new AccountContext())
            {
                showMainMenu();
                db.Database.Migrate();
                while (true)
                {

                    string selectedMenuOption = Console.ReadLine();

                    if (selectedMenuOption == "2")
                    {
                        showMainMenuTwo();
                    }


                    if (selectedMenuOption == "1")
                    {
                        openAccount(db);
                        showMainMenu();

                    }


                    if (selectedMenuOption == "2.1")
                    {
                        withdrawFromAccount(db);
                        showMainMenu();
                    }

                    else if (selectedMenuOption == "2.2")
                    {
                        depositToAccount(db);
                        showMainMenu();
                    }


                    else if (selectedMenuOption == "3")
                    {
                        viewAccount(db);
                        showMainMenu();
                    }
                    else if (selectedMenuOption == "4")
                    {
                        closeAccount(db);
                        showMainMenu();
                    }
                    else if (selectedMenuOption == "quit")
                    {
                        break;
                    }

                }
            }
        }

    }

    public static class Helper
    {
        public static String GetInnerMessages(this Exception exception, int? maxDepth = null)
        {
            if (maxDepth == null) maxDepth = 5;
            StringBuilder message = new StringBuilder();
            message.Append(exception.Message);
            if (exception.InnerException != null && maxDepth > 0)
            {
                message.AppendFormat("with Inner Exception: {0}", exception.InnerException.GetInnerMessages(--maxDepth));
                return message.ToString();
            }
            else
            {
                return message.ToString();
            }
        }
    }


}




